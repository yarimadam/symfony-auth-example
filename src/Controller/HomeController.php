<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    /**
     * @Route(path="/home", name="home")
     * @param Request $request
     * @return Response
     */
    public function home(Request $request)
    {
        return $this->render('userDetails.html.twig');
    }

    /**
     * @Route(path="/login", name="login")
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        $form = $this->createForm('App\Form\LoginForm');
        $form->handleRequest($request);

        $formView = $form->createView();

        $viewData = ['form' => $formView];

        if ($form->isSubmitted()) {
            $viewData['error'] = $request->getSession()->get('authenticationError') ? true : false;
        }

        return $this->render('login.html.twig', $viewData);
    }

    /**
     * @Route(path="/logout", name="logut")
     */
    public function logout()
    {
    }
}
