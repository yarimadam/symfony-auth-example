<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class,
                ['constraints' =>
                    [
                        new Length(['min' => 5]),
                    ]
                ]
            )
            ->add('password', PasswordType::class,
                [
                    'constraints' => [
                        new Length(['min' => 5])
                    ]
                ]
            );
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
