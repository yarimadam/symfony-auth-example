<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Halil');
        $user->setEmail('halil@ubit.com.tr');
        $user->setAge(27);
        $user->setPassword('qwerty');

        $manager->persist($user);
        $manager->flush();
    }
}
